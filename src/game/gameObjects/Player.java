package game.gameObjects;

import game.panels.Game;
import game.GameFrame;
import game.options.Options;
import game.utils.Images;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Player implements GameObject, KeyListener {
    private final ArrayList<Bullet> bullets;
    private int posX = 0;
    private int posY = 0;

    public int momX = (int) (Options.scale);
    public int momY = (int) (5 * Options.scale);

    private Image[] animationFrames = Images.player;
    private boolean shooting = false;

    private Game game;

    private boolean remove = false;
    private long lastShoot = System.currentTimeMillis();

    private int fireRate = 3;
    private boolean jumping = false;
    private boolean isOnGround = true;
    private boolean isSpacePressed = false;
    private boolean falling = false;
    private int frameCounter = 0;
    private int animationFrameCounter;
    private int animationSpeed = 2;
    private boolean speedMode = false;
    private int oldMomX;
    private boolean blockJump = false;

    public Player(Game game, ArrayList<Bullet> bullets) {
        this.game = game;
        this.bullets = bullets;
    }

    private void jump() {
        if (isOnGround && !isSpacePressed) {
            jumping = true;
            isOnGround = false;
            isSpacePressed = true;
            momY = (int) (-2.5 * Options.scale);
        } if(jumping && !isOnGround) {
            momY += 1* Options.scale;
            if(momY == 0) {
                falling = true;
                jumping = false;
            }
        }
    }

    private void stopJump() {
        isSpacePressed = false;
        jumping = false;
        falling = true;
    }

    private void shoot() {
        shooting = true;
    }

    private void stopShoot() {
        shooting = false;
    }

    public void setPosX(int x) {
        posX = x;
    }

    public void setPosY(int y) {
        posY = y;
    }

    @Override
    public void update() {
        if(posX < 0) {
            markForRemoval();
        } if(posY > GameFrame.windowHeight + Options.gameObjectHeight) {
            markForRemoval();
        }
        posX += momX;
        moveY();
        if(shooting && System.currentTimeMillis() - lastShoot >= 1000/fireRate) {
            game.spawnBullet(posX+Options.gameObjectWidth, posY+Options.gameObjectHeight/2);
            lastShoot = System.currentTimeMillis();
        }
        for(Bullet bullet : bullets) {
            if(bullet.getRectangle().intersects(getRectangle())) {
                markForRemoval();
            }
        }
    }

    private void moveY() {
        if(falling) {
            momY += 1 * Options.scale;
            if(momY > 5 * Options.scale) {
                momY = (int) (5 * Options.scale);
            }
        }
        posY += momY;
    }

    @Override
    public void draw(Graphics g) {
        g.drawImage(animationFrames[animationFrameCounter% animationFrames.length], posX, posY, Options.gameObjectWidth, Options.gameObjectHeight,null);
        if(Options.debug) {
            g.setColor(Color.BLUE);
            fillRectRect(g, getBottomRect());
            fillRectRect(g, getTopRect());
            fillRectRect(g, getRightRect());
        }
        if(frameCounter % animationSpeed == 0) {
            animationFrameCounter++;
        }
        frameCounter++;
    }

    private void fillRectRect(Graphics g, Rectangle rect) {
        g.fillRect(rect.x,rect.y,rect.width,rect.height);
    }

    public Rectangle getRightRect() {
        return new Rectangle(posX+Options.gameObjectWidth,posY+Options.gameObjectHeight/2-1,1,1);
    }

    public Rectangle getTopRect() {
        return new Rectangle((int)(posX+Options.scale),posY-1,(int)(Options.gameObjectWidth - 2*Options.scale),1);
    }

    public Rectangle getBottomRect() {
        return new Rectangle((int)(posX+Options.scale),posY+Options.gameObjectHeight,(int)(Options.gameObjectWidth - 2*Options.scale),1);
    }

    public void groundTrigger() {
        isOnGround = true;
        falling = false;
        blockJump = false;
        momY = (int) (5 * Options.scale);
    }

    @Override
    public boolean isToBeRemoved() {
        return remove;
    }

    @Override
    public Rectangle getRectangle() {
        return new Rectangle(posX,posY,Options.gameObjectWidth,Options.gameObjectHeight);
    }

    @Override
    public void markForRemoval() {
        game.stopGameLoop();
        remove = true;
    }

    @Override
    public void scroll(int speed) {
        posX -= speed;
    }

    @Override
    public void keyTyped(KeyEvent e){

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_SPACE) {
            shoot();
            if(!blockJump) {
                jump();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_SPACE) {
            stopShoot();
            stopJump();
        }
    }

    public void changeMomX(double factor, int time) {
        oldMomX = momX;
        speedMode = true;
        momX += factor * Options.scale;
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                momX = oldMomX;
                speedMode = false;
            }
        }, time);
    }

    public void setSpeedDown() {
        if(speedMode) {
            momX = oldMomX;
            speedMode = false;
        }
    }

    public void superJump() {
        int oldMomY = momY;
        speedMode = true;
        momY = 0;
        momY -= 3 * Options.scale;
        Timer timer = new Timer();
        blockJump = true;
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                momY = oldMomY;
                speedMode = false;
            }
        }, 500);
    }
}
