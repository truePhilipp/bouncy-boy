package game.gameObjects.powerUps;

import game.gameObjects.Bullet;
import game.gameObjects.Ground;
import game.gameObjects.Player;
import game.gameObjects.enemies.Enemy;
import game.utils.Images;

import java.util.ArrayList;

public class SpeedBlock extends Ground {
    private boolean isActivated = false;

    public SpeedBlock(int x, int y, ArrayList<Enemy> enemies, ArrayList<Bullet> bullets, Player player) {
        super(x, y, enemies, bullets, player, Images.speed);
    }

    @Override
    public void update() {
        super.update();
        if (!isActivated && player.getBottomRect().intersects(getRectangle())) {
            player.changeMomX(0.5,5000);
            isActivated = true;
        }
    }
}
