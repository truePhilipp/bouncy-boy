package game.gameObjects.powerUps;

import game.gameObjects.Bullet;
import game.gameObjects.Ground;
import game.gameObjects.Player;
import game.gameObjects.enemies.Enemy;
import game.utils.Images;

import java.util.ArrayList;

public class BreakingBlock extends Ground {
    public BreakingBlock(int x, int y, ArrayList<Enemy> enemies, ArrayList<Bullet> bullets, Player player) {
        super(x, y, enemies, bullets, player, Images.breakingBlock);
    }

    @Override
    public void update() {
        super.update();
        for(Bullet bullet : bullets) {
            if(bullet.getRectangle().intersects(getRectangle())) {
                bullet.markForRemoval();
                markForRemoval();
            }
        }
    }
}
