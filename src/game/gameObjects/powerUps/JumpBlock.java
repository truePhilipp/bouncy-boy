package game.gameObjects.powerUps;

import game.gameObjects.Bullet;
import game.gameObjects.Ground;
import game.gameObjects.Player;
import game.gameObjects.enemies.Enemy;
import game.utils.Images;

import java.util.ArrayList;

public class JumpBlock extends Ground {
    public JumpBlock(int x, int y, ArrayList<Enemy> enemies, ArrayList<Bullet> bullets, Player player) {
        super(x, y, enemies, bullets, player, Images.jump);
    }

    @Override
    public void update() {
        super.update();
        if (player.getBottomRect().intersects(getRectangle())) {
            player.superJump();
        }
    }
}
