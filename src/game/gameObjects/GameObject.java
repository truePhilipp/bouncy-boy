package game.gameObjects;

import game.options.Options;

import java.awt.*;

public interface GameObject {
    void update();
    void draw(Graphics g);
    void scroll(int speed);
    Rectangle getRectangle();
    void markForRemoval();
    boolean isToBeRemoved();
}
