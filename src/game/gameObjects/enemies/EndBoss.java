package game.gameObjects.enemies;

import game.GameFrame;
import game.gameObjects.Bullet;
import game.gameObjects.Ground;
import game.gameObjects.Player;
import game.options.Options;
import game.panels.Credits;
import game.utils.SoundEffect;
import game.utils.SoundEffectsPlayer;

import java.awt.*;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class EndBoss extends Enemy {
    private final GameFrame gameFrame;
    private int ownWidth = (int) (40 * Options.scale);
    private int ownHeight = (int) (40 * Options.scale);
    private int lives = 30;
    private boolean musicStarted = false;

    public EndBoss(int x, int y, ArrayList<Ground> walls, ArrayList<Bullet> bullets, Player player, Image[] animationFrames, GameFrame gameFrame) {
        super(x, y, walls, animationFrames, bullets, player);
        this.gameFrame = gameFrame;
        momX = (int) Options.scale;
    }

    @Override
    public void update() {
        if(posX < GameFrame.windowWidth-2*ownWidth) {
            if(!musicStarted) {
                SoundEffectsPlayer sfxPlayer = SoundEffectsPlayer.getInstance();
                sfxPlayer.stop();
                sfxPlayer.play(SoundEffect.BOSS);
                musicStarted = true;
            }
            posX += momX;
        }
        for(Bullet bullet : bullets) {
            if(bullet.getRectangle().intersects(getRectangle())) {
                bullet.markForRemoval();
                lives--;
                if(lives <= 0) {
                    markForRemoval();
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            gameFrame.changePanel(new Credits(gameFrame));
                        }
                    }, 3000);
                }
            }
        }
    }

    @Override
    public void draw(Graphics g) {
        if(posX < GameFrame.windowWidth) {
            g.drawImage(animationFrames[animationFrameCounter % animationFrames.length], posX, posY, ownWidth, ownHeight, null);
            if (frameCounter % animationSpeed == 0) {
                animationFrameCounter++;
            }
            if (frameCounter % 30 == 0) {
                shoot();
            }
            frameCounter++;
        }
    }

    private void shoot() {
        Bullet bullet = new Bullet(posX - 10, (int) (posY + ownHeight / 2 + Math.sin(frameCounter) * ownHeight/2));
        bullet.reverse();
        bullets.add(bullet);
    }

    @Override
    public void scroll(int speed) {
        posX -= speed;
    }

    @Override
    public Rectangle getRectangle() {
        return new Rectangle(posX,posY,ownWidth,ownHeight);
    }
}
