package game.gameObjects.enemies;

import game.GameFrame;
import game.gameObjects.Bullet;
import game.gameObjects.GameObject;
import game.gameObjects.Ground;
import game.gameObjects.Player;
import game.options.Options;

import java.awt.*;
import java.util.ArrayList;

public class FlyingEnemy extends Enemy {
    public FlyingEnemy(int x, int y, ArrayList<Ground> walls, ArrayList<Bullet> bullets, Player player, Image[] animationFrames) {
        super(x, y, walls, animationFrames, bullets, player);
        momX = (int) -Options.scale;
    }

    @Override
    public void update() {
        super.update();
        if(posX < GameFrame.windowWidth) {
            posX += momX;
        }
    }
}
