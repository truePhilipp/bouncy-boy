package game.gameObjects.enemies;

import game.GameFrame;
import game.gameObjects.Bullet;
import game.gameObjects.GameObject;
import game.gameObjects.Ground;
import game.gameObjects.Player;
import game.options.Options;

import java.awt.*;
import java.util.ArrayList;

public abstract class Enemy implements GameObject {
    protected final ArrayList<Bullet> bullets;
    protected Image[] animationFrames = null;
    protected int posX, posY, momX, momY;
    private boolean remove = false;
    private ArrayList<Ground> walls;
    protected int frameCounter;
    protected int animationSpeed = 2;
    protected int animationFrameCounter;
    private Player player;

    public Enemy(int x, int y, ArrayList<Ground> walls, Image[] animationFrames, ArrayList<Bullet> bullets, Player player) {
        posX = x;
        posY = y;
        this.walls = walls;
        this.bullets = bullets;
        this.player = player;
        this.animationFrames = animationFrames;
    }

    @Override
    public void update() {
        if(posX < -Options.gameObjectWidth) {
            markForRemoval();
        }
        if(posX < GameFrame.windowWidth) {
            if(player.getRectangle().intersects(getRectangle())) {
                player.markForRemoval();
            }
            for(Bullet bullet : bullets) {
                if(bullet.getRectangle().intersects(getRectangle())) {
                    bullet.markForRemoval();
                    markForRemoval();
                }
            }
        }
    }

    @Override
    public void draw(Graphics g) {
        g.drawImage(animationFrames[animationFrameCounter%animationFrames.length], posX, posY, Options.gameObjectWidth, Options.gameObjectHeight,null);
        if(Options.debug) {
            g.setColor(Color.BLUE);
            fillRectRect(g, getBottomRect());
            fillRectRect(g, getLeftRect());
            fillRectRect(g, getRightRect());
        }
        if(frameCounter % animationSpeed == 0) {
            animationFrameCounter++;
        }
        frameCounter++;
    }

    private void fillRectRect(Graphics g, Rectangle rect) {
        g.fillRect(rect.x,rect.y,rect.width,rect.height);
    }

    @Override
    public void scroll(int speed) {
        posX -= speed;
    }

    public void setPosX(int x) {
        posX = x;
    }

    public void setPosY(int y) {
        posY = y;
    }

    public Rectangle getRightRect() {
        return new Rectangle(posX+Options.gameObjectWidth,posY+Options.gameObjectHeight/2-1,1,2);
    }

    public Rectangle getLeftRect() {
        return new Rectangle(posX-1,posY+Options.gameObjectHeight/2-1,1,2);
    }

    public Rectangle getBottomRect() {
        return new Rectangle((int)(posX+Options.scale),posY+Options.gameObjectHeight,(int)(Options.gameObjectWidth - 2*Options.scale),1);
    }

    @Override
    public Rectangle getRectangle() {
        return new Rectangle(posX,posY,Options.gameObjectWidth,Options.gameObjectHeight);
    }

    @Override
    public void markForRemoval() {
        remove = true;
    }

    @Override
    public boolean isToBeRemoved() {
        return remove;
    }

    public void turn() {
        momX = -momX;
    }
}
