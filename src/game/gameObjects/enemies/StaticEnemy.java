package game.gameObjects.enemies;

import game.gameObjects.Bullet;
import game.gameObjects.GameObject;
import game.gameObjects.Ground;
import game.gameObjects.Player;

import java.awt.*;
import java.util.ArrayList;

public class StaticEnemy extends Enemy {

    public StaticEnemy(int x, int y, ArrayList<Ground> walls, ArrayList<Bullet> bullets, Player player, Image[] animationFrame) {
        super(x, y, walls, animationFrame, bullets, player);
    }
}
