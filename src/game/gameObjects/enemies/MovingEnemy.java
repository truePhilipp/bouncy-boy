package game.gameObjects.enemies;

import game.GameFrame;
import game.gameObjects.Bullet;
import game.gameObjects.Ground;
import game.gameObjects.Player;
import game.options.Options;

import java.awt.*;
import java.util.ArrayList;

public class MovingEnemy extends Enemy {
    public MovingEnemy(int x, int y, ArrayList<Ground> walls, ArrayList<Bullet> bullets, Player player, Image[] animationFrame) {
        super(x, y, walls, animationFrame, bullets, player);
        momX = (int) -Options.scale;
        momY = (int) (5 * Options.scale);
    }

    @Override
    public void update() {
        super.update();
        if(posX < GameFrame.windowWidth) {
            posX += momX;
            posY += momY;
        }
    }
}
