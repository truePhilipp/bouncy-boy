package game.gameObjects;

import game.GameFrame;
import game.gameObjects.enemies.Enemy;
import game.gameObjects.enemies.FlyingEnemy;
import game.gameObjects.enemies.MovingEnemy;
import game.options.Options;

import java.awt.*;
import java.util.ArrayList;

public class Ground implements GameObject {
    protected final ArrayList<Bullet> bullets;
    private Image[] animationFrames;
    private ArrayList<Enemy> enemies;
    protected Player player;
    protected int posX;
    protected int posY;

    private boolean remove = false;
    private int animationFrameCounter;
    private int frameCounter;
    private int animationSpeed = 30;

    public Ground(int x, int y, ArrayList<Enemy> enemies, ArrayList<Bullet> bullets, Player player, Image[] animationFrames) {
        posX = x;
        posY = y;
        this.enemies = enemies;
        this.bullets = bullets;
        this.player = player;
        this.animationFrames = animationFrames;
    }

    @Override
    public void update() {
        if(posX < -Options.gameObjectWidth) {
            markForRemoval();
        }

        if(posX < GameFrame.windowWidth) {
            if (player.getRightRect().intersects(getRectangle())) {
                player.setPosX(posX - Options.gameObjectWidth);
                player.setSpeedDown();
            }
            if (player.getBottomRect().intersects(getRectangle())) {
                player.setPosY(posY - Options.gameObjectHeight);
                player.groundTrigger();
            }
            if (player.getTopRect().intersects(getRectangle())) {
                player.setPosY(posY + Options.gameObjectHeight);
            }
            for(Bullet bullet : bullets) {
                if(bullet.getRectangle().intersects(getRectangle())) {
                    bullet.markForRemoval();
                }
            }
            for (Enemy enemy : enemies) {
                if(enemy instanceof MovingEnemy) {
                    if (enemy.getRightRect().intersects(getRectangle())) {
                        enemy.turn();
                    }
                    if (enemy.getLeftRect().intersects(getRectangle())) {
                        enemy.turn();
                    }
                    if (enemy.getBottomRect().intersects(getRectangle())) {
                        //*1.5 because of some unknown bug || maybe fix later
                        enemy.setPosY((int) (posY - 1.5*Options.gameObjectHeight));
                    }
                }
                if(enemy instanceof  FlyingEnemy) {
                    if (enemy.getRightRect().intersects(getRectangle())) {
                        enemy.turn();
                    }
                    if (enemy.getLeftRect().intersects(getRectangle())) {
                        enemy.turn();
                    }
                }
            }
        }
    }

    @Override
    public void draw(Graphics g) {
        g.drawImage(animationFrames[animationFrameCounter%animationFrames.length], posX, posY, Options.gameObjectWidth, Options.gameObjectHeight,null);
        if(frameCounter % animationSpeed == 0) {
            animationFrameCounter++;
        }
        frameCounter++;
    }

    @Override
    public Rectangle getRectangle() {
        return new Rectangle(posX,posY,Options.gameObjectWidth,Options.gameObjectHeight);
    }

    @Override
    public void markForRemoval() {
        remove = true;
    }

    @Override
    public boolean isToBeRemoved() {
        return remove;
    }

    @Override
    public void scroll(int speed) {
        posX -= speed;
    }
}
