package game.gameObjects;

import game.GameFrame;
import game.options.Options;

import java.awt.*;

public class Bullet implements GameObject {
    private int posX, posY;
    private int momX = (int) (4 * Options.scale);
    private boolean remove = false;

    private int ownWidth = (int) (3 * Options.scale);
    private int ownHeight = (int) (3 * Options.scale);

    public Bullet(int x, int y) {
        posX = x;
        posY = y;
    }

    @Override
    public void update() {
        posX += momX;
        if(posX > GameFrame.windowWidth) {
            remove = true;
        }
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(posX,posY,ownWidth,ownHeight);
    }

    @Override
    public void scroll(int speed) {
        posX -= speed;
    }

    @Override
    public Rectangle getRectangle() {
        return new Rectangle(posX,posY,ownWidth,ownHeight);
    }

    @Override
    public void markForRemoval() {
        remove = true;
    }

    @Override
    public boolean isToBeRemoved() {
        return remove;
    }

    public void reverse() {
        momX = -momX;
    }
}
