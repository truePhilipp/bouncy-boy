package game.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Images {
    public final static Image[] dirt = fillArray("blocks/dirt");
    public final static Image[] grass = fillArray("blocks/grass");
    public final static Image[] jump = fillArray("blocks/jump");
    public final static Image[] nonType = fillArray("blocks/nonType");
    public final static Image[] slow = fillArray("blocks/slow");
    public final static Image[] speed = fillArray("blocks/speed");
    public final static Image[] stone = fillArray("blocks/stone");
    public final static Image[] breakingBlock = fillArray("blocks/breakingBlock");

    public final static Image[] fly = fillArray("enemies/fly");
    public final static Image[] lava = fillArray("enemies/lava");
    public final static Image[] spike = fillArray("enemies/spike");
    public final static Image[] unihorn = fillArray("enemies/unihorn");

    public final static Image[] endBoss = fillArray("enemies/endBoss");

    public final static Image[] player = fillArray("player");

    private static Image[] fillArray(String path) {
        Image[] imgArray = new Image[0];
        try {
            File directory = new File("res/img/"+path);
            File[] files;
            files = directory.listFiles();
            imgArray = new Image[files.length];
            for(int i = 0; i<files.length; i++) {
                imgArray[i] = ImageIO.read(files[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imgArray;
    }
}
