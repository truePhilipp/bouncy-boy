package game.utils;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SoundEffectsPlayer {

	private Map<SoundEffect, Clip> soundEffects = new HashMap<>();

	private static SoundEffectsPlayer instance = new SoundEffectsPlayer();
	private SoundEffect lastPlayed;

	public static SoundEffectsPlayer getInstance() {
		return instance;
	}

	private SoundEffectsPlayer() {
		loadSoundEffect(SoundEffect.TITLE, "res/sfx/music/title.wav");
		loadSoundEffect(SoundEffect.LEVEL, "res/sfx/music/level.wav");
		loadSoundEffect(SoundEffect.BOSS, "res/sfx/music/boss.wav");
		loadSoundEffect(SoundEffect.CREDITS, "res/sfx/music/credits.wav");
		loadSoundEffect(SoundEffect.GAMEOVER, "res/sfx/music/gameOver.wav");
	}

	public enum Volume {
		MUTE, HIGH
	}

	public Volume volume = Volume.HIGH;

	private void loadSoundEffect(SoundEffect sfx, String filename) {
		try {
			Clip clip = AudioSystem.getClip();
			AudioInputStream ais = AudioSystem.getAudioInputStream(new File(filename));
			clip.open(ais);
			soundEffects.put(sfx, clip);
		} catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	public void play(SoundEffect sfx) {
		lastPlayed = sfx;
		if (volume != Volume.MUTE) {
			Clip sound = soundEffects.get(sfx);
			if (!sound.isRunning()) {
				sound.setFramePosition(0);
				sound.start();
			}
		}
	}

	public void stop() {
		Clip sound = soundEffects.get(lastPlayed);
		if(sound != null) {
			sound.stop();
		}
	}

}
