package game.utils;

import java.io.*;

public class LevelLoader {
    public int[][] loadLevel(String name) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File("res/level/"+name+".txt")));
        int[][] data = new int[24][];

        int i = 0;
        String line;
        while((line = reader.readLine()) != null) {

            String[] rawData = line.split(",");
            int length = rawData.length;
            int[] lineData = new int[length];
            for(int j = 0; j<rawData.length; j++) {
                if (rawData[j].equals("")) {
                    lineData[j] = 0;
                } else {
                    lineData[j] = Integer.valueOf(rawData[j]);
                }
            }
            data[i] = lineData;
            i++;
        }
        return data;
    }
}
