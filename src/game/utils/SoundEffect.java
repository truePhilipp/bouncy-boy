package game.utils;

public enum SoundEffect {
	TITLE, LEVEL, BOSS, CREDITS, GAMEOVER
}
