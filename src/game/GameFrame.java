package game;

import game.options.Options;
import game.panels.TitleScreen;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GameFrame extends JFrame {
    private JPanel activePanel;

    public static int windowWidth = 32 * Options.gameObjectWidth;
    public static int windowHeight = 24 * Options.gameObjectHeight;

    public GameFrame() {
        super();
        setTitle("Gmtk 2019");
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                System.exit(0);
            }
        });

        changePanel(new TitleScreen(this));

        if(!Options.debug) {
            setResizable(false);
        }
        setVisible(true);
    }

    public void changePanel(JPanel panel) {
        if(activePanel != null) {
            remove(activePanel);
        }
        activePanel = panel;
        activePanel.setFocusable(true);
        activePanel.requestFocus();
        activePanel.setPreferredSize(new Dimension(windowWidth, windowHeight));
        add(activePanel);
        pack();
    }

    public static JButton createButton(String type) throws IOException {
        JButton button = new JButton();

        BufferedImage image;
        image = ImageIO.read(new File("res/img/menu/"+type+".png"));
        button.setPreferredSize(new Dimension((int) (image.getWidth() * game.options.Options.scale),(int) (image.getHeight() * game.options.Options.scale)));
        button.setIcon(new ImageIcon( image.getScaledInstance((int) (image.getWidth() * game.options.Options.scale),(int) (image.getHeight() * game.options.Options.scale), Image.SCALE_FAST)));
        button.setBorder(BorderFactory.createEmptyBorder());
        button.setContentAreaFilled(false);

        return button;
    }
}
