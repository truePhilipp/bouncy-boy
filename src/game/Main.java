package game;

import game.options.Options;
import game.utils.SoundEffectsPlayer;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        Options.loadOptions();
        SoundEffectsPlayer.getInstance();
        SwingUtilities.invokeLater(() -> new GameFrame());
    }
}
