package game.options;

import javax.imageio.IIOException;
import java.io.*;
import java.util.Properties;
import java.util.SplittableRandom;

public abstract class Options {
    public static double scale = 2.0;
    public static double music = 0.5;
    public static double sfx = 0.5;
    public static boolean debug = true;

    public static int gameObjectWidth = (int) (10 * scale);
    public static int gameObjectHeight = (int) (10 * scale);

    public static void loadOptions() {
        try {
            File optionsFile = new File("options.properties");
            if(!optionsFile.isFile()) {
                saveOptions();
            }
            FileInputStream fileStream = new FileInputStream(optionsFile);
            Properties props = new Properties();
            props.load(fileStream);
            scale = Double.valueOf(props.getProperty("scale"));
            music = Double.valueOf(props.getProperty("music"));
            sfx = Double.valueOf(props.getProperty("sfx"));
            debug = Boolean.valueOf(props.getProperty("debug"));

            gameObjectWidth = (int) (10 * scale);
            gameObjectHeight = (int) (10 * scale);

            fileStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void saveOptions() {
        try {
            File optionsFile = new File("options.properties");
            if (!optionsFile.isFile()) {
                optionsFile.createNewFile();
            }
            Properties props = new Properties();
            props.setProperty("scale", String.valueOf(scale));
            props.setProperty("music", String.valueOf(music));
            props.setProperty("sfx", String.valueOf(sfx));
            props.setProperty("debug", String.valueOf(debug));

            FileOutputStream fileStream = new FileOutputStream(optionsFile);
            props.store(fileStream, "Don't mess with these properties. Or do, I don't care. Maybe make a backup.\n");
            fileStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
