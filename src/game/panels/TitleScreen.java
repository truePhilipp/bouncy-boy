package game.panels;

import game.GameFrame;
import game.utils.SoundEffect;
import game.utils.SoundEffectsPlayer;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class TitleScreen extends JPanel {
    public TitleScreen(GameFrame gameFrame) {
        super();
        SoundEffectsPlayer sfxPlayer = SoundEffectsPlayer.getInstance();
        sfxPlayer.stop();
        sfxPlayer.play(SoundEffect.TITLE);
        try {
            JButton logo = GameFrame.createButton("logo");
            JButton start = GameFrame.createButton("start");
            JButton options = GameFrame.createButton("options");
            JButton end = GameFrame.createButton("end");

            start.addActionListener(e -> gameFrame.changePanel(new Game(gameFrame)));
            options.addActionListener(e -> gameFrame.changePanel(new Options(gameFrame)));
            end.addActionListener(e -> System.exit(0));

            Container out = new Container();
            out.setLayout(new GridLayout(0,1));

            Container container = new Container();
            container.setLayout(new GridLayout(0,1));

            container.add(start);
            container.add(new JLabel());
            container.add(options);
            container.add(new JLabel());
            container.add(end);

            out.add(logo);
            out.add(container);
            out.add(new JLabel());

            add(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
