package game.panels;

import game.GameFrame;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Options extends JPanel {
    public Options(GameFrame gameFrame) {
        super();
        try {
            JButton wSize = GameFrame.createButton("wsrr");
            JButton music = GameFrame.createButton("music");
            JButton sfx = GameFrame.createButton("sfx");

            JSlider sizeSlider = new JSlider(200,400,(int) game.options.Options.scale*100);
            JSlider musicSlider = new JSlider(0,10,(int) game.options.Options.music*10);
            JSlider sfxSlider = new JSlider(0,10,(int) game.options.Options.sfx*10);

            JButton save = GameFrame.createButton("save");
            JButton back = GameFrame.createButton("back");

            save.addActionListener(e -> {
                game.options.Options.scale = sizeSlider.getValue()/100.0;
                game.options.Options.music = musicSlider.getValue()/10.0;
                game.options.Options.sfx = sfxSlider.getValue()/10.0;
                game.options.Options.saveOptions();

                gameFrame.changePanel(new TitleScreen(gameFrame));
            });

            back.addActionListener(e -> gameFrame.changePanel(new TitleScreen(gameFrame)));

            Container all = new Container();
            all.setLayout(new GridLayout(0,1));

            Container row1 = new Container();
            Container row2 = new Container();
            Container row3 = new Container();
            Container row4 = new Container();

            row1.setLayout(new GridLayout(1,2));
            row2.setLayout(new GridLayout(1,2));
            row3.setLayout(new GridLayout(1,2));
            row4.setLayout(new GridLayout(1,2));

            row1.add(wSize);
            row1.add(sizeSlider);
            row2.add(music);
            row2.add(musicSlider);
            row3.add(sfx);
            row3.add(sfxSlider);
            row4.add(save);
            row4.add(back);

            all.add(row1);
            all.add(new JLabel());
            all.add(row2);
            all.add(new JLabel());
            all.add(row3);
            all.add(new JLabel());
            all.add(row4);

            add(all);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
