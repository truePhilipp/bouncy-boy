package game.panels;

import game.GameFrame;
import game.utils.SoundEffect;
import game.utils.SoundEffectsPlayer;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Credits extends JPanel {

  private final GameFrame gameFrame;
  int width = GameFrame.windowWidth;
  int height = GameFrame.windowHeight;

  String[] texts = {
          "",
          "Credits",
          "",
          "Programmers",
          "Philipp Mayr",
          "Alexander Plei",
          "",
          "Graphics",
          "Jeremias Forssman",
          "Maximilian Leß",
          "",
          "Leveldesign",
          "Jeremias Forssman",
          "Maximilian Leß",
          "",
          "Music",
          "Kevin McLeod",
          "Musique de jeu video"
  };

  int offset;
  int size = 130;

  int textHeight = texts.length*size;
  boolean done = false;
  private int counter = 0;


  public Credits(GameFrame gameFrame) {
    SoundEffectsPlayer sfxPlayer = SoundEffectsPlayer.getInstance();
    sfxPlayer.stop();
    sfxPlayer.play(SoundEffect.CREDITS);

    this.gameFrame = gameFrame;
    offset = height;
    setSize(width, height);

    GraphicsEnvironment ge =
            GraphicsEnvironment.getLocalGraphicsEnvironment();
    try {
      ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/PressStart2P-Regular.ttf")));
    } catch (FontFormatException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    run();
  }

  public void run() {
    Thread loop = new Thread(() -> {
      while(!done) {
        repaint();
        try {
          Thread.sleep(1000/180);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      gameFrame.changePanel(new TitleScreen(gameFrame));
    });
    loop.start();
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.setColor(Color.decode("#999999"));
    g.fillRect(0,0,width, height);

    Graphics2D g2d = (Graphics2D) g;

    g.setColor(Color.decode("#333333"));
    
    for(int i = 0; i < texts.length; i++) {
      String text = texts[i];

      int textWidth = g.getFontMetrics().stringWidth(text);
      g.setFont(new Font("PressStart2P-Regular.ttf", Font.PLAIN, size));
      g2d.drawString(text, width/2- textWidth/2,(i * size) + offset);
    }
    offset--;
    counter++;
    System.out.println("###############");
    System.out.println(counter);
    System.out.println(textHeight + height);
    if(counter > textHeight + height){
      done = true;
    }
  }
}
