package game.panels;

import javax.swing.*;
import game.GameFrame;
import game.utils.SoundEffect;
import game.utils.SoundEffectsPlayer;

import java.awt.*;
import java.io.IOException;

public class GameOver extends JPanel {
    public GameOver(GameFrame gameFrame) {
        super();
        SoundEffectsPlayer sfxPlayer = SoundEffectsPlayer.getInstance();
        sfxPlayer.stop();
        sfxPlayer.play(SoundEffect.GAMEOVER);
        try {
            JButton gameOverLabel = GameFrame.createButton("gameOver");

            JButton restartButton = GameFrame.createButton("restart");
            JButton endButton = GameFrame.createButton("backToMenu");

            restartButton.addActionListener(e -> gameFrame.changePanel(new Game(gameFrame)));
            endButton.addActionListener(e -> gameFrame.changePanel(new TitleScreen(gameFrame)));

            Container container = new Container();
            container.setLayout(new GridLayout(0,1));

            container.add(gameOverLabel);
            container.add(new JLabel());
            container.add(restartButton);
            container.add(new JLabel());
            container.add(endButton);

            add(container);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
