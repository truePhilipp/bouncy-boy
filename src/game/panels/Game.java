package game.panels;

import game.GameFrame;
import game.gameObjects.Bullet;
import game.gameObjects.enemies.*;
import game.gameObjects.GameObject;
import game.gameObjects.Ground;
import game.gameObjects.Player;
import game.gameObjects.powerUps.BreakingBlock;
import game.gameObjects.powerUps.JumpBlock;
import game.gameObjects.powerUps.SlowBlock;
import game.gameObjects.powerUps.SpeedBlock;
import game.options.Options;
import game.utils.Images;
import game.utils.LevelLoader;
import game.utils.SoundEffect;
import game.utils.SoundEffectsPlayer;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class Game extends JPanel {
    private final Player player;
    private int frameRate = 60;

    private LevelLoader levelLoader = new LevelLoader();

    private GameLoop gameLoop = new GameLoop();

    private ArrayList<Ground> walls = new ArrayList<>();
    private ArrayList<Ground> decoWalls = new ArrayList<>();
    private ArrayList<Enemy> enemies = new ArrayList<>();
    private ArrayList<Bullet> bullets = new ArrayList<>();

    private GameFrame gameFrame;

    private boolean scrolling = true;

    public Game(GameFrame gameFrame) {
        SoundEffectsPlayer sfxPlayer = SoundEffectsPlayer.getInstance();
        sfxPlayer.stop();
        sfxPlayer.play(SoundEffect.LEVEL);

        this.gameFrame = gameFrame;
        gameFrame.add(this);
        player = new Player(this, bullets);
        addKeyListener(player);
        loadLevel(player);

        gameLoop.start();
    }

    private void loadLevel(Player player) {
        int[][] level = null;
        try {
            level = levelLoader.loadLevel("lvl1");
        } catch (IOException e) {
            System.out.println("Level could not be loaded.");
            System.exit(1);
        }

        for(int y = 0; y<level.length; y++) {
            for(int x = 0; x<level[y].length; x++) {
                switch (level[y][x]) {
                    case -1:
                        player.setPosX(x*Options.gameObjectWidth);
                        player.setPosY(y*Options.gameObjectHeight);
                        break;
                    case 0:
                        break;
                    case 1:
                        walls.add(new Ground(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player, Images.nonType));
                        break;
                    case 2:
                        walls.add(new Ground(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player, Images.stone));
                        break;
                    case 5:
                        walls.add(new Ground(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player, Images.grass));
                        break;
                    case 6:
                        walls.add(new Ground(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player, Images.dirt));
                        break;
                    case 7:
                        walls.add(new BreakingBlock(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player));
                        break;
                    case 50:
                        walls.add(new SpeedBlock(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player));
                        break;
                    case 51:
                        walls.add(new SlowBlock(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player));
                        break;
                    case 52:
                        walls.add(new JumpBlock(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player));
                        break;
                    case 75:
                        enemies.add(new StaticEnemy(x*Options.gameObjectWidth,y*Options.gameObjectHeight, walls, bullets, player, Images.spike));
                        break;
                    case 76:
                        enemies.add(new MovingEnemy(x*Options.gameObjectWidth,y*Options.gameObjectHeight, walls, bullets, player, Images.unihorn));
                        break;
                    case 77:
                        enemies.add(new FlyingEnemy(x*Options.gameObjectWidth,y*Options.gameObjectHeight, walls, bullets, player, Images.fly));
                        break;
                    case 78:
                        enemies.add(new StaticEnemy(x*Options.gameObjectWidth,y*Options.gameObjectHeight, walls, bullets, player, Images.lava));
                        break;
                    case 80:
                        enemies.add(new EndBoss(x*Options.gameObjectWidth,y*Options.gameObjectHeight, walls, bullets, player, Images.endBoss, gameFrame));
                        break;
                    case 101:
                        decoWalls.add(new Ground(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player,Images.nonType));
                        break;
                    case 102:
                        decoWalls.add(new Ground(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player,Images.stone));
                        break;
                    case 105:
                        decoWalls.add(new Ground(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player,Images.grass));
                        break;
                    case 106:
                        decoWalls.add(new Ground(x*Options.gameObjectWidth,y*Options.gameObjectHeight, enemies, bullets, player,Images.dirt));
                        break;
                }
            }
        }
    }

    public void spawnBullet(int x, int y) {
        bullets.add(new Bullet(x,y));
        //sfxPlayer.play(SoundEffect.GUN);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for(int i = 0; i < walls.size(); i++) {
            walls.get(i).draw(g);
        }
        for(int i = 0; i < decoWalls.size(); i++) {
            decoWalls.get(i).draw(g);
        }
        for(int i = 0; i < enemies.size(); i++) {
            enemies.get(i).draw(g);
        }
        for(int i = 0; i < bullets.size(); i++) {
            bullets.get(i).draw(g);
        }
        player.draw(g);
    }

    public void update() {
        player.update();
        for(int i = 0; i < walls.size(); i++) {
            walls.get(i).update();
        }
        for(int i = 0; i < enemies.size(); i++) {
            enemies.get(i).update();
        }
        for(int i = 0; i < bullets.size(); i++) {
            bullets.get(i).update();
        }
    }

    public void scroll(int delta) {
        player.scroll((int) (delta * Options.scale));
        for(int i = 0; i < walls.size(); i++) {
            walls.get(i).scroll((int) (delta * Options.scale));
        }
        for(int i = 0; i < decoWalls.size(); i++) {
            decoWalls.get(i).scroll((int) (delta * Options.scale));
        }
        for(int i = 0; i < enemies.size(); i++) {
            enemies.get(i).scroll((int) (delta * Options.scale));
        }
        for(int i = 0; i < bullets.size(); i++) {
            bullets.get(i).scroll((int) (delta * Options.scale));
        }
    }

    private void removeObjects() {
        for(int i = walls.size() - 1; i >= 0; i--) {
            GameObject go = walls.get(i);
            if(go.isToBeRemoved()) {
                walls.remove(go);
            }
        }
        for(int i = enemies.size() - 1; i >= 0; i--) {
            Enemy enemy = enemies.get(i);
            if(enemy.isToBeRemoved()) {
                enemies.remove(enemy);
            }
        }
        for(int i = bullets.size() - 1; i >= 0; i--) {
            Bullet bullet = bullets.get(i);
            if(bullet.isToBeRemoved()) {
                bullets.remove(bullet);
            }
        }
    }

    public void stopGameLoop() {
        gameLoop.stopGameLoop();
    }

    private class GameLoop extends Thread {
        private boolean running = true;

        @Override
        public void run() {
            if(Options.debug) {
                scroll(2000);
            }
            while(running) {
                update();
                if(scrolling) {
                    scroll(1);
                }
                repaint();
                removeObjects();
                try {
                    Thread.sleep(1000/frameRate);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            gameFrame.changePanel(new GameOver(gameFrame));
        }

        public void stopGameLoop() {
            running = false;
        }
    }
}
